package cpe.projet.erudilyon.wscode.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "T_E_CODE_CODE")
@Data
public class Code implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CODE_ID")
    @JsonIgnore
    private Integer id;

    @Column(name = "QR_CODE")
    private String qrCode;

    @Column(name = "DATE_CREA_CODE")
    private Date dateCreaCode;

    @Column(name = "DATE_CONSO_CODE")
    private Date dateConsoCode;

    @Column(name = "USER_CONSO_CODE")
    private Integer userConsoCode;

    @Column(name = "ACTIVITY")
    private Integer activity;

    public Code() {
    }
}
