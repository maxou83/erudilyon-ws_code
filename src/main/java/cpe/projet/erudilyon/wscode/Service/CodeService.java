package cpe.projet.erudilyon.wscode.Service;
import Model.ActivityDTO;
import Model.CodeDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wscode.Model.Code;
import cpe.projet.erudilyon.wscode.Repository.CodeRepository;
import cpe.projet.erudilyon.wscode.Utils.CodeGenerator;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class CodeService {

    @Autowired
    private CodeRepository codeRepository;

    @Autowired
    private RestService restService;

    public String genererCode(ActivityDTO activityDTO) throws Exception {

        // On génère un code unique
        String codeGen = CodeGenerator.generateRandomString(7);

        // On transforme le codeDTO en code
        Code code = new Code();

        code.setDateCreaCode(new Date());
        code.setActivity(activityDTO.getId());
        code.setQrCode(codeGen);

        codeRepository.save(code);
        return codeGen;
    }

    public Code getCode(CodeDTO codeDTO){
        return codeRepository.findCodeByQrCode(codeDTO.getCode());
    }

    public DataValidation invalidateCode(CodeDTO codeDTO) {

        //Objet de validation
        DataValidation dataValidation = new DataValidation();

        // Récupérer l'objet CODE en base
        Code code = this.getCode(codeDTO);

        // Si aucun code n'est retourné, on invalide
        if(code == null)
            return new DataValidation(Code.class.getName(), false);

        // On assigne l'id d'activité au DTO
        codeDTO.setActivityId(code.getActivity());
        System.out.println("[LOG] Assignation de l'ID de l'activité à l'objet \"code\" ");

        // On demande au WS Activité de valider si l'activité est ok
        DataValidation activityValidation = restService.validateActivity(codeDTO);
        System.out.println("[LOG] Validation 'OK' pour les dates de l'activité");

        // Si c'est valide
        if(activityValidation.isValid()){

            // On récupère l'activité
            ActivityDTO activityDTO = restService.getActivityByCode(codeDTO);
            System.out.println("[LOG] Récupération 'OK' de l'activité");

            if(code.getDateConsoCode() == null && code.getUserConsoCode() == null){

                // On change les informations de code (date conso et user)
                this.deactivateCode(code, codeDTO.getUserId());
                System.out.println("[LOG] Désactivation 'OK' du code -> code maintenant considéré comme consommé");

                // On met à jour les infos de l'utilisateur (solde et cumul)
                restService.updateUserSolde(codeDTO.getUserId(), activityDTO.getGainActivity());
                System.out.println("[LOG] Mise à jour 'OK' du solde de l'utilisateur");

                // On débloque un défi
                // TODO: appel rest pour le déblocage du défi
                // System.out.println("[LOG] Mise à jour 'OK' du solde de l'utilisateur");

                dataValidation.setStatus("OK_CONSUMED:" + activityDTO.getGainActivity());
                dataValidation.setValid(true);
                return dataValidation;

            } else {
                System.out.println("[LOG] Code déjà consommé");

                dataValidation.setStatus("ALREADY_CONSUMED");
                dataValidation.setValid(false);
                return dataValidation;
            }

        } else {
            System.out.println("[LOG] Activité non valide");

            dataValidation.setStatus("INVALID_ACTIVITY");
            dataValidation.setValid(false);
            return dataValidation;
        }
    }

    private boolean deactivateCode(Code code, Integer userId){
        Code c = codeRepository.findById(code.getId()).get();
        c.setDateConsoCode(new Date());
        c.setUserConsoCode(userId);
        codeRepository.save(c);
        return true;
    }

    public List<Code> getAllCode(){
        List<Code> codeList = new ArrayList<>();
        codeRepository.findAll().forEach(codeList::add);
        return codeList;
    }
}
