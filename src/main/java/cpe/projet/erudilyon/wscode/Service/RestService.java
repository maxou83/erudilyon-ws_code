package cpe.projet.erudilyon.wscode.Service;

import Model.ActivityDTO;
import Model.ActivityUserTransactionDTO;
import Model.CodeDTO;
import Model.UserDTO;
import Validation.DataValidation;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collections;

@Service
public class RestService {

    private final RestTemplate restTemplate;
    //private final String baseUrl = "http://localhost";
    private final String baseUrl = "http://asi-projet-cpe.northeurope.cloudapp.azure.com";

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public DataValidation validateActivity(CodeDTO codeDTO) {

        String url = baseUrl + ":8083/validateByCode";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");
        HttpEntity<CodeDTO> entity = new HttpEntity<>(codeDTO, headers);

        return restTemplate.postForObject(url, entity, DataValidation.class);
    }

    public ActivityDTO getActivityByCode(CodeDTO codeDTO) {

        String url = baseUrl + ":8083/getActivityByCode";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");
        HttpEntity<CodeDTO> entity = new HttpEntity<>(codeDTO, headers);

        return restTemplate.postForObject(url, entity, ActivityDTO.class);

    }

    public UserDTO updateUserSolde(Integer userId, BigDecimal activityId) {

        String url =  baseUrl + ":8084/addSolde";

        ActivityUserTransactionDTO transaction = new ActivityUserTransactionDTO();
        transaction.setUserId(userId);
        transaction.setAmount(activityId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth("admin", "admin");

        HttpEntity<ActivityUserTransactionDTO> entity = new HttpEntity(transaction, headers);

        return restTemplate.postForObject(url, entity, UserDTO.class);

    }


}