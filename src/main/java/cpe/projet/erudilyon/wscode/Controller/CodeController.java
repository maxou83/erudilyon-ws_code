package cpe.projet.erudilyon.wscode.Controller;

import Model.ActivityDTO;
import Model.CodeDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wscode.Model.Code;
import cpe.projet.erudilyon.wscode.Service.CodeService;
import cpe.projet.erudilyon.wscode.Service.RestService;
import cpe.projet.erudilyon.wscode.Utils.ZXingHelper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

@RestController
@Api(value="API de Gestion des Codes", description="Gestion des QRCodes relatifs aux Activités")
public class CodeController {

    @Autowired
    private CodeService codeService;

    @Autowired
    private RestService restService;


    @GetMapping(value = "/generate")
    public void generateQrCode(@RequestBody ActivityDTO activityDTO, HttpServletResponse response) throws Exception
    {
        String text = codeService.genererCode(activityDTO);

        response.setContentType("image/png");
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(ZXingHelper.getQRCodeImage(text, 200, 200));
        outputStream.flush();
        outputStream.close();
    }


    @GetMapping(value = "/all")
    public List<Code> getAllCode() throws Exception
    {
        return codeService.getAllCode();
    }

    @PostMapping(value = "/validate")
    public DataValidation validateCode(@RequestBody CodeDTO codeDTO){
        return codeService.invalidateCode(codeDTO);
    }

}
