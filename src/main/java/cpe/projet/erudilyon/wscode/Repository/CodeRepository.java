package cpe.projet.erudilyon.wscode.Repository;
import cpe.projet.erudilyon.wscode.Model.Code;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CodeRepository extends CrudRepository<Code, Integer> {

    Code findCodeByQrCode(String qrCode);

    //@Query(value = "select exists (select 1 from t_e_code_code where qr_code=?1) AS \"exists\" ;")
    //String qrCodeExists(String qr);

    Boolean existsCodeByQrCode(String qr);



}
