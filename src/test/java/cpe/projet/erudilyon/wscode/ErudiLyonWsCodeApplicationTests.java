package cpe.projet.erudilyon.wscode;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = ErudiLyonWsCodeApplication.class)
@DataJpaTest
class ErudiLyonWsCodeApplicationTests {

    @Test
    void contextLoads() {
    }

}
