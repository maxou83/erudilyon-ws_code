FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/ws-code-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8082
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]